const env = require('./env.js');
 
const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  dialect: env.dialect,
  operatorsAliases: false,
 
  pool: {
    max: env.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle
  }
});
const db = {};
 
db.Sequelize = Sequelize;
db.sequelize = sequelize;
 
//Models/tables
db.customers = require('../model/customer.model.js')(sequelize, Sequelize);
db.servicios = require('../model/servicio.model.js')(sequelize, Sequelize);
db.cargos = require ("../model/cargo.model.js")(sequelize, Sequelize);
db.empresas = require("../model/empresa.model.js")(sequelize, Sequelize);
db.sucursales = require("../model/sucursal.model.js")(sequelize, Sequelize);
module.exports = db

/*const env = require('./env.js');
 
const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  dialect: env.dialect,
  operatorsAliases: false,
 
  pool: {
    max: env.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle
  }
});
const db = {};
 
db.Sequelize = Sequelize;
db.sequelize = sequelize;
 
//Models/tables
db.customers = require('../model/customer.model.js')(sequelize, Sequelize);
 
module.exports = db
*/

/*const env = require('./env.js');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(
env.database,
env.username,
env.host,
env.password,                              
{
	host: env.host,
	dialect: env.dialect,
	operatorsAliases: false,

	pool: {
		max: env.max,
		min: env.pool.min,
		acquire: env.pool.acquire,
	}
});


const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize; 

//modelos de las tablas 
db.customers = require ('../model/customer.model.js')(sequelize, Sequelize);
module.exports = db; */