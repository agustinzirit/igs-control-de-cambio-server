const db = require('../config/db.config.js');
const Empresa = db.empresas;
const Sucursal = db.sucursales;
const env = require('../config/env.js');

const Sequelize = require('sequelize');
const sequelize = db.sequelize;

// FETCH All Customers
exports.findAll = (req, res) => {
  Empresa.findAll().then(empresa => {
      // Send All empresas to Client
      res.json(empresa.sort(function(c1, c2){return c1.id - c2.id}));
    }).catch(err => {
      console.log(err);
      res.status(500).json({msg: "error", details: err});
    });
};


//Borrar un cargo por id
exports.delete = ( req , res ) => {
	const id = req.params.id;
	Empresa.destroy({
		where: { id: id }
	}).then(() => {
		res.status(200).json({msg: "Se elimino la empresa correctamente"});
	})
};


//FindByID(Nuevo)
exports.findById = (req , res) => {
	Empresa.findByPk(req.params.id).then(empresa => {
		res.json(empresa);
	}).catch( err => {
		console.log(err);
		res.status(500).json( {msg: "error", details: err});
	});
};


//Update cargo (Nuevo)
exports.update = ( req, res ) => {
	const id = req.body.Id_Empresa; 
	console.log("Este es el id:",id);
	Empresa.update( req.body,
              {where: {Id_Empresa: id } }).then(() => {
              	res.status(200).json( { mgs: "Update Successfully -> Empresa Id = " + id});
              }).catch(err => {
              	console.log(err);
              	res.status(500).json( {msg: "error", details: err});
              });
};




exports.create = ( req, res ) => {
	//Guarda en la bd de Postgresql

	return sequelize.transaction(t => {
		console.log(req.body);
		console.log(req.body.Nombre_empresa);
		console.log(req.body.Rif_empresa);
		console.log(req.body.sucursales[0].Nombre_sucursal);
		console.log(req.body.sucursales[0].Telefono_sucursal);


		return Empresa.create({
			Nombre_empresa : req.body.Nombre_empresa,
			Rif_empresa : req.body.Rif_empresa,
			//"Estado_empresa" : req.body.Estado_empresa,
		}, {transaction: t}).then(empresa => {

			console.log(empresa.Id_Empresa);
			req.body.sucursales[].Id_Empresa = empresa.Id_Empresa;
			var sucursales = req.body.sucursales;
			console.log("Este es el dato", sucursales);
			//ForEach y meter el dato
		 return Sucursal.bulkCreate(sucursales, 
		                            {fields: ["Id_empresa",'Nombre_sucursal', "Telefono_sucursal"]}, 
		                            {transaction: t});
		});
		}).then(empresa => {
			//Envia la empresa creada al cliente
			res.json(empresa);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error ", details : err})
		});

		/* Empresa.create({
			"Nombre_empresa" : req.body.Nombre_empresa,
			"Rif_empresa" : req.body.Rif_empresa,
			"Estado_empresa" : req.body.Estado_empresa,
		}).then(empresa => {
			//Envia la empresa creada al cliente
			res.json(empresa);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error ", details : err})
		});  */
};



return sequelize.transaction(t => {

  // chain all your queries here. make sure you return them.
  return User.create({
    firstName: 'Abraham',
    lastName: 'Lincoln'
  }, {transaction: t}).then(user => {
    return user.setShooter({
      firstName: 'John',
      lastName: 'Boothe'
    }, {transaction: t});
  });

}).then(result => {
  // Transaction has been committed
  // result is whatever the result of the promise chain returned to the transaction callback
}).catch(err => {
  // Transaction has been rolled back
  // err is whatever rejected the promise chain returned to the transaction callback
});