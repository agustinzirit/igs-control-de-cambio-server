const db = require('../config/db.config.js');
const Sucursal = db.sucursales;
const env = require('../config/env.js');

const Sequelize = require('sequelize');
const sequelize = db.sequelize;

exports.create = ( req, res ) => {
	//Guarda en la bd de Postgresql
	Sucursal.create({
		"Nombre_sucursal" : req.body.Nombre_sucursal,
		//El id no es estatico
		"Id_empresa" : req.body.Id_empresa,
		"Estado_sucursal" : req.body.Estado_sucursal,
	}).then(sucursal => {
		//Envia el servicio creado al cliente
		res.json(sucursal);
	}).catch(err => {
		console.log(err);
		res.status(500).json({msg: "error ", details : err})
	});
};