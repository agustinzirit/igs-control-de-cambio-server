var express = require('express');
var app = express(); 
var bodyParser = require('body-parser');
app.use(bodyParser.json());


const cors = require('cors');
const corsOption = {
	origin: 'http://localhost:4200',
	optionsSuccessStatus: 200
};
app.use(cors(corsOption));

const db = require('./app/config/db.config.js')

//Force : true will drop the table if it already exists

db.sequelize.sync({force : true}).then(() => {
	console.log("Drop And Resync with {force : true}");
  Servicioss();
  Cargos();
	initial();
  Empresas();
  Sucursales();

});

require("./app/route/customer.route.js")(app);
require("./app/route/servicio.route.js")(app);
require("./app/route/cargo.route.js")(app);
require("./app/route/empresa.route.js")(app);
require("./app/route/sucursal.route.js")(app);

//Create a Server 
 var server =  app.listen(8081, function () {

 	var host = server.address().address; 
 	var port = server.address().port; 

 	console.log("App Listening at http://%s:%s", host, port);
 })


function Sucursales(){
  let sucursales = [{
     Nombre_sucursal: "Primera",
     Rif_empresa: "1231232321",
     Estado_empresa: true,
     Id_empresa: 1 },
     {
     Nombre_sucursal: "Segunda",
     Rif_empresa: "1231232321",
     Estado_empresa: true,
     Id_empresa: 3  },
     {
     Nombre_sucursal: "Tercera",
     Rif_empresa: "1231232321",
     Estado_empresa: false ,
     Id_empresa: 2 }
  ]
  const Sucursales = db.sucursales;
  for (let i = 0; i < sucursales.length; i++){
    Sucursales.create(sucursales[i]);
  }
}

function Empresas(){
  let empresas = [
  {Nombre_empresa: "McDonalds",
   Rif_empresa: "1231232321",
   Estado_empresa: true },
  {Nombre_empresa: "Sambil",
   Rif_empresa: "13333333",
   Estado_empresa: false },
  {Nombre_empresa: "Sigo",
   Rif_empresa: "1111111111",
   Estado_empresa: true }
  ]
   const Empresa = db.empresas;
    for (let i = 0; i < empresas.length; i++) {
    Empresa.create(empresas[i]);
  }
}


function Cargos(){
  let cargos = [
  {
    Nombre_cargo: "Desarrollador",
    Descripcion_cargo: "Se encarga de programar paginas webs para clientes",
    Estado_cargo: false
 },
 {
    Nombre_cargo: "Administrador de base de datos",
    Descripcion_cargo: "Se encarga de administrar base de datos",
    Estado_cargo: true
 },{
   Nombre_cargo: "Analista de sistemas",
    Descripcion_cargo: "Se encarga de idear el desarrollo de los software a desarrollar",
    Estado_cargo: true
 }
  ]
    const Cargo = db.cargos;
    for (let i = 0; i < cargos.length; i++) {
    Cargo.create(cargos[i]);
  }
};


function Servicioss(){
let servicios = [
    {
      Nombre_servicio: "Desarrollo web",
      Descripcion_servicio: "Se encarga del desarrollo de apicaciones agiles en el entorno web",
      Estado_servicio: false
    },
    {
      Nombre_servicio: "Administracion de BD",
      Descripcion_servicio: "Se encarga de la administracion de las bases de datos",
      Estado_servicio: true
    },
    {
      Nombre_servicio: "Telefonia",
      Descripcion_servicio: "Sevicios aplicaciones a los servicios telefonicos",
      Estado_servicio: false
    }
  ]
    const Servicio = db.servicios;
  for (let i = 0; i < servicios.length; i++) {
    Servicio.create(servicios[i]);
  }
};



 function initial(){
 
  let customers = [
    {
      firstname: "Joe",
      lastname: "Thomas",
      age: 36
    },
    {
      firstname: "Peter",
      lastname: "Smith",
      age: 18
    },
    {
      firstname: "Lauren",
      lastname: "Taylor",
      age: 31
    },
    {
      firstname: "Mary",
      lastname: "Taylor",
      age: 24
    },
    {
      firstname: "David",
      lastname: "Moore",
      age: 25
    },
    {
      firstname: "Holly",
      lastname: "Davies",
      age: 27
    },
    {
      firstname: "Michael",
      lastname: "Brown",
      age: 45
    }
  ]
 
  // Init data -> save to PostgreSQL
  const Customer = db.customers;
  for (let i = 0; i < customers.length; i++) { 
    Customer.create(customers[i]);  
  }
}